# INFO630PHP

## Setup Instructions

1. **Clone the Repository**  
   Clone the repo to your machine using your preferred method (SSH or HTTPS).

2. **Install Docker and Docker-Compose**  
   Ensure you have Docker and Docker-Compose installed on your machine. 
   - For Docker, visit [Docker's official website](https://www.docker.com/get-started).
   - For Docker-Compose, follow the instructions at [Docker Compose](https://docs.docker.com/compose/install/).

3. **Start the Project**  
   Run the following command in your terminal:

docker-compose up -d

This command starts the containers in detached mode.

4. **Access the Project**  
The project will be running on [http://localhost/](http://localhost/).

