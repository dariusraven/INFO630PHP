<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once('lib.php');
//echo 'Welcome ' . $_SESSION['username'] . ' to TaskApp!';
echo '<link rel="stylesheet" href="style.css">';
echo '<link rel="stylesheet" href="menu.css">';
//
//<div id='logout' class='logout'><a href="logout.php">Logout</a></div>
//

//if($_SESSION['role'] == 'admin'){
//    echo '<div id="app_header" class="app_header">ADMIN HEADER:  welcome '.$_SESSION['username'].'</div>';
//}
//else{
//    echo '<div id="app_header" class="app_header"">USER HEADER: welcome: '.$_SESSION['username'].'</div>';

    //echo '<div style="width: 100%; background-color: \'red\'"> Welcome '.$_SESSION['username'].' to TaskApp</div>';
    //require 'f_create_task.php';
echo '<div class="menu-bar">';
echo '<ul class="menu-list">';
echo '<li><a href="/homepage.php">Home</a></li>';
echo '<li><a href="/UserGuide.pdf">User Guide</a></li>';
echo  '</ul>';
echo   '<div class="user-info">';
echo   '<span class="username">' . $_SESSION['username'] . '</span>';
echo   '<a href="/profile.php">Profile</a>';
echo   '<a href="/logout.php">Log Out</a>';
echo    '</div>';
echo '</div>';

$isAdmin = 0;
if($_SESSION['role'] == 'admin'){
    $isAdmin = 1;
}

$breadcrumbs = array();
if(str_contains($_SERVER['REQUEST_URI'],'homepage' )){
    array_push($breadcrumbs,'Homepage');
}
if(str_contains($_SERVER['REQUEST_URI'],'list_tasks' )){
    if($isAdmin){array_push($breadcrumbs,$_GET['username']);}
    array_push($breadcrumbs,'Task List');
}

if(str_contains($_SERVER['REQUEST_URI'],'task.php' ) && !str_contains($_SERVER['REQUEST_URI'],'create')){
    if($isAdmin){array_push($breadcrumbs,$_GET['username']);}
    array_push($breadcrumbs,'Edit Task');
    array_push($breadcrumbs,$_REQUEST['task_id']);
}



echo '<div id="breadcrumbs">' . join('->',$breadcrumbs) . '</div>';
show_message();
//}