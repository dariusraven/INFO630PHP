<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Inactive</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        h2 {
            color: #333;
            text-align: center;
            margin-top: 50px;
        }

        p {
            color: #555;
            font-size: 18px;
            text-align: center;
            margin-top: 20px;
        }

        a {
            display: block;
            width: 120px;
            margin: 20px auto;
            padding: 10px 20px;
            background-color: #4caf50; 
            color: #fff;
            text-align: center;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s;
        }

        a:hover {
            background-color: #388e3c; 
        }
    </style>
</head>
<body>
    <h2>User Inactive</h2>
    <p>This account is currently inactive.</p>
    <p>Please contact an Administrator or register a new account.</p>
    <a href="f_register.php">Register</a> 
</body>
</html>
