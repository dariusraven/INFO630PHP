<?php

require_once('header.php');
require_once('db.php');
require_once('lib.php');

if($_SESSION['role'] == 'user'){
    echo 'you do not have permission to view this page';
    exit();
}

$search = "";
if (!empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
}

$page = 1;
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
if ($page < 1){
    $page = 1;
}

$pagesize = 5;
if (!empty($_GET['pagesize'])) {
    $pagesize = $_GET['pagesize'];
}

$inactive = 0;
if (!empty($_REQUEST['inactive'])) {
    $inactive = $_REQUEST['inactive'];
}

//fetch tasks from database
$data = get_users($search,!$inactive,$page,$pagesize);
$users = $data['users'];
$rows = $data['rows'];
$pages = $data['pages'];

?>

<div id='toggle'><a href='f_create_user.php'><button>Create New User</button></a></div>
</br>
<div id='search'>
<form method='post' action='list_users.php'>
    <input type=text name='search' id='search' value='<?php echo $search?>'></input>
    <button type='submit'>Search</button>
</form>
</div>

<div id='user_list'>
    <table id="userTable">
        <thead>
            <tr>
                <th onclick="sortTable(0)">Username</th>
                <th onclick="sortTable(1)">Role</th>
                <th onclick="sortTable(2)">First Name</th>
                <th onclick="sortTable(3)">Last Name</th>
                <th onclick="sortTable(4)">Email</th>
                <th onclick="sortTable(4)">Active</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php


            foreach ($users as $user){
                $username = $user['username'];
                echo "<tr>";
                echo "<td>" . htmlspecialchars($user['username']) . "</td>";
                echo "<td>" . htmlspecialchars($user['role']) . "</td>";
                echo "<td>" . htmlspecialchars($user['first_name']) . "</td>";
                echo "<td>" . htmlspecialchars($user['last_name']) . "</td>";
                echo "<td>" . htmlspecialchars($user['email']) . "</td>";
                echo "<td>" . htmlspecialchars($user['active']) . "</td>";
                echo "<td><a href='user.php?username=$username'><button>Edit</button></a></td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>
<div id='pagination'>
        <?php
        echo "Current Page: $page ";
        //only show pagination if more than one page worth of content
        if($pages > 1){
            // show previous page icon if not on first page
            if($page != 1){
                echo "<a href='list_users.php?search=".$search."&inactive=$inactive&page=".($page-1)."'><< </a>";
            }
            // list links to all pages
            for($x = 1;$x <= $pages; $x++){
                echo "<a href='list_users.php?search=".$search."&inactive=$inactive&page=$x'> $x</a>";
            }
            // show next page icon if not last page
            if($page != $pages){
                echo "<a href='list_users.php?search=".$search."&inactive=$inactive&page=".($page+1)."'> >></a>";
            }
        }
        ?>
    </div>
    <?php


    if($inactive ==0){
        
        echo("<div id='toggle'><a href='list_users.php?inactive=1'><button>Show Inactive Users</button></a></div>");
    }
    else{
        echo("<div id='toggle'><a href='list_users.php?inactive=0'><button>Show Active Users</button></a></div>");
    }
    ?>



<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("userTable");
        switching = true;
        dir = "asc"; 
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch= true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++;      
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }

        var headers = document.querySelectorAll('#userTable th');
        headers.forEach(function(header) {
            header.classList.remove('sorted', 'asc', 'desc');
        });

        var currentHeader = document.getElementById("userTable").rows[0].cells[n];
        currentHeader.classList.add('sorted');

        if (dir === "asc") {
            currentHeader.classList.add('asc');
        } else {
            currentHeader.classList.add('desc');
        }
    }
</script>


</body>
</html>
