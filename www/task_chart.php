<?php
require_once('lib.php');

$username = $_SESSION['username'];
if (!empty($_GET['username']) && ($_SESSION['role']!='user')) {
    $username = $_GET['username'];
}
$categories = get_categories_for_user($username);

?>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=500, initial-scale=1.0">
    <title>Pie Chart Example</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>    
<div id="chartContainer">
        <canvas id="myPieChart"></canvas>
    </div>
</html>
<script>
const ctx = document.getElementById('myPieChart').getContext('2d');
const myPieChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['School', 'Work', 'Personal'],
        datasets: [{
            label: 'Tasks',
            data: [<?php echo $categories['school']?>, <?php echo $categories['work']?>, <?php echo $categories['personal']?>],
            backgroundColor: [
                
                'rgba(54, 162, 235, 0.2)',
                
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
               
                'rgba(54, 162, 235, 1)',
                
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            }
        }
    }
});
</script>
