<?php
require_once('db.php');
$pdo = get_pdo();

$_SESSION['message'] = '';

function user_message($message){
    //echo "<div class='user_message'>" . $message . "</div>";
    $_SESSION['message'] = $message;
}
function show_message(){
    echo "<div class='user_message'>" . $_SESSION['message'] . "</div>";
    $_SESSION['message'] = "";
}
function send_email($address, $subject, $body){
    $pdo = get_pdo();
    $stmt = $pdo->prepare("insert into emails (address, subject,body) values (?,?,?)");
    $stmt->execute([$address,$subject,$body]);
}
function get_tasks($username, $search,$completed,$page,$pagesize){
    if($completed){
        $where = " username = ? and status = 'complete' and (TITLE like ? OR description like ?)";
    }
    else{
        $where = " username = ? and status != 'complete' and (TITLE like ? OR description like ?)";
    }
    $pdo = get_pdo();

    $stmt = $pdo->prepare("SELECT count(*) as CNT,CEIL(COUNT(*)/?) as pages FROM tasks WHERE " . $where);
    $stmt->execute([$pagesize,$username, '%' . $search . '%','%'.$search.'%']);
    $dcount = $stmt->fetch();
    

    $stmt = $pdo->prepare("SELECT * FROM tasks WHERE " . $where ." limit ? offset ?");
    $stmt->execute([$username, '%' . $search . '%','%'.$search.'%',$pagesize,(($page-1)*$pagesize)]);
    $tasks = $stmt->fetchAll();
    return array('rows'=>$dcount['CNT'],'tasks'=>$tasks,'pages'=>$dcount['pages']);    
}

function get_users($search,$active,$page,$pagesize){
    
    $where = " active = ? and username like ?";

    $pdo = get_pdo();

    $stmt = $pdo->prepare("SELECT count(*) as CNT,CEIL(COUNT(*)/?) as pages FROM users WHERE " . $where);
    $stmt->execute([$pagesize,$active,'%' . $search . '%']);
    $dcount = $stmt->fetch();
    

    $stmt = $pdo->prepare("SELECT * FROM users WHERE " . $where ." limit ? offset ?");
    $stmt->execute([$active,'%' . $search . '%',$pagesize,(($page-1)*$pagesize)]);
    $tasks = $stmt->fetchAll();
    return array('rows'=>$dcount['CNT'],'users'=>$tasks,'pages'=>$dcount['pages']);    
}

function get_categories_for_user($username){
    $pdo = get_pdo();

    $stmt = $pdo->prepare("select category, COUNT(*) as CNT from tasks where username = ? group by category");
    $stmt->execute([$username]);
    $data = $stmt->fetchAll();
    
    $return = array('personal'=>0,'work'=>0,'school'=>0);

    foreach($data as $row){
        $return[$row['category']] = $row['CNT'];
    }

    return $return;
}