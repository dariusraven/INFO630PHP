<?php
require_once('header.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>User Creation Form</title>
</head>
<body>
<div id='create_user_form' class='form_container'>
    <h2>Create User Form</h2>

    <form action="s_create_user.php" method="post">
        <div>
            <label for="username">Name:</label>
            <input type="text" name="username" id="username" required>
        </div>
        <div>
            <label for="password">Password:</label>
            <input type="password" name="password" id="password" required>
        </div>
        <div>
            <label for="password2">Confirm Password:</label>
            <input type="password" name="password2" id="password2" required>
        </div>
        <div>
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" id="first_name" required >
    </div>
    <div>
        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" id="last_name" required >
    </div>
    <div>
        <label for="email">Email:</label>
        <input type="text" name="email" id="email" required>
    </div>

    <div>
        <label for="role">Role:</label>
            <select name="role" id="role">
                <option value="admin">Admin</option>
                <option value="user">User</option>
        </select>
    </div>
        <button type="submit">Create User</button></form><a href='homepage.php'></a>
    </form>
</div>
</body>
</html>
