<?php

require_once('header.php');
require_once('db.php');

if (!empty($_POST['first_name'])) {
    $stmt = $pdo->prepare("UPDATE users SET first_name = ?, last_name = ?, email = ? WHERE username = ?");
    $stmt->execute([$_POST['first_name'], $_POST['last_name'], $_POST['email'], $_SESSION['username']]);
}

$stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
$stmt->execute([$_SESSION['username']]);
$user = $stmt->fetch();

?>
<!DOCTYPE html>
<html>
<head>
    <title>User Profile</title>
   
</head>
<body>
<h2>User Profile</h2>

<div class = form_container>

<form action="profile.php" method="post">
    <div>
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" id="first_name" required value="<?php echo($user['first_name']) ?>">
    </div>
    <div>
        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" id="last_name" required value="<?php echo($user['last_name']) ?>">
    </div>
    <div>
        <label for="email">Email:</label>
        <input type="text" name="email" id="email" required value="<?php echo($user['email']) ?>">
    </div>
    <button type="submit">Save</button>
    <a href='homepage.php'><button type="button">Cancel</button></a>
</form>
    </div>
</body>
</html>

<?php require_once('task_chart.php');