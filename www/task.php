<?php

require_once('header.php');
require_once('db.php');

$task_id = '';
if (!empty($_POST['task_id'])) {
    $task_id = $_POST['task_id'];
}

if (!empty($_GET['task_id'])) {
    $task_id = $_GET['task_id'];
}

// Check if task_id is set before accessing the database
if ($task_id) {
    $stmt = $pdo->prepare("SELECT * FROM tasks where task_id = ?");
    $stmt->execute([$task_id]);
    $task = $stmt->fetch();
} else {
    // Initialize $task to avoid undefined variable warning
    $task = array(
        'task_id' => '',
        'title' => '',
        'description' => '',
        'status' => '',
        'priority' => '',
        'category' => '',
        'duedate' => ''
    );
}

// Check task_name
if (!empty($_POST['title'])) {
    $complete_text = '';
    if($_POST['status'] == 'complete'){$complete_text = ', datecompleted=NOW() ';}
    $stmt = $pdo->prepare("UPDATE tasks SET title = ?, description = ?, priority = ?, status = ?, category = ? $complete_text WHERE task_id = ?");
    
    $stmt->execute([$_POST['title'], $_POST['description'], $_POST['priority'], $_POST['status'], $_POST['category'], $task_id]);
    user_message('Task Saved!');
    echo '<script>window.location.href = "homepage.php";</script>';
}

?>
<!DOCTYPE html>
<html>

<head>
    <title>Modify Task Form</title>

</head>
<body>
<div id='modify_task_form'  class='form_container'>
    <h2>Modify Task Form</h2>

    <form action="task.php" method="post">
        <input type="hidden" name="task_id" value="<?php echo $task['task_id']; ?>">
        <div>
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" value="<?php echo $task['title']; ?>" required>
        </div>
        <div>
            <label for="description">Description:</label><br>
            <textarea name="description" id="description" maxlength="400" required><?php echo $task['description']; ?></textarea>
        </div>
        <div>
            <label for="status">Status:</label>
            <select name="status" id="status">
                <option value="new" <?php if ($task['status'] == 'new') echo 'selected'; ?>>New</option>
                <option value="in_progress" <?php if ($task['status'] == 'in_progress') echo 'selected'; ?>>In Progress</option>
                <option value="complete" <?php if ($task['status'] == 'complete') echo 'selected'; ?>>Completed</option>
            </select>
        </div>
        <div>
            <label for="priority">Priority:</label>
            <select name="priority" id="priority">
                <option value="3" <?php if ($task['priority'] == '3') echo 'selected'; ?>>Low</option>
                <option value="2" <?php if ($task['priority'] == '2') echo 'selected'; ?>>Medium</option>
                <option value="1" <?php if ($task['priority'] == '1') echo 'selected'; ?>>High</option>
            </select>
        </div>
        <div>
            <label for="category">Category:</label>
            <select name="category" id="category">
                <option value="personal" <?php if ($task['category'] == 'personal') echo 'selected'; ?>>Personal</option>
                <option value="work" <?php if ($task['category'] == 'work') echo 'selected'; ?>>Work</option>
                <option value="school" <?php if ($task['category'] == 'school') echo 'selected'; ?>>School</option>
            </select>
        </div>
        <div>
        <label for="duedate">Due Date:</label>
        <input type="date" id="duedate" name="duedate" required value='<?php echo $task['duedate'];?>'>
        </div>
        <button type="submit">Save</button>
    </form>
    <a href='homepage.php'><button>Cancel</button></a>

</div>
</body>
</html>

