<?php
require_once('lib.php');
require_once('header.php');
require_once('db.php');

    $username = $_POST['username'];
    $password = $_POST['password'];
    $password2 = $_POST['password2'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $role = $_POST['role'];
    $active = $_POST['active'];

    if ($password != $password2) {
        echo "Error: Passwords do not match.";
    } else {


        $stmt = $pdo->prepare("insert into users (username, passhash, role, first_name, last_name, email, active) values (?,?,?,?,?,?,?)");

        if ($stmt->execute([$username,password_hash($password, PASSWORD_DEFAULT),$role,$first_name,$last_name,$email,$active])) {
            echo "User created successfully.";
        } else {
            echo "Error: " . $conn->error;
        }

    }

require 'homepage.php';
exit();