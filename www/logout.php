<?php
include_once('lib.php');

session_start();
$_SESSION['username'] = '';
$_SESSION['role'] = '';
session_destroy();
user_message("You have successfully logged out!");
include('f_login.php');
