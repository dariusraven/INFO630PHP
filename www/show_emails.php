<?php
require_once('db.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Task Table</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;
        }
    </style>
</head>
<body>
<div id='list_emails' class='list_emails'>
<table id="emailTable">
    <thead>
        <tr>
            <th onclick="sortTable(0)">ID</th>
            <th onclick="sortTable(1)">Address</th>
            <th onclick="sortTable(2)">Subject</th>
            <th onclick="sortTable(3)">Body</th>
        </tr>
    </thead>
    <tbody>
<?php

 $stmt = $pdo->prepare("SELECT * from emails");
        $stmt->execute([]);
        $tasks = $stmt->fetchAll();

        foreach ($tasks as $task) {
            echo "<tr>";
            echo "<td>" . htmlspecialchars($task['id']) . "</td>";
            echo "<td>" . htmlspecialchars($task['address']) . "</td>";
            echo "<td>" . htmlspecialchars($task['subject']) . "</td>";
            echo "<td>" . htmlspecialchars($task['body']) . "</td>";
            echo "</tr>";
        }
?>
    </tbody>
</table>
</div>
<script>
function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("taskTable");
    switching = true;
    dir = "asc"; 
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch= true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount ++;      
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}
</script>

</body>
</html>
