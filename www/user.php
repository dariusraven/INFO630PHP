<?php

require_once('header.php');
require_once('db.php');

if (!empty($_POST['save'])) {
    $stmt = $pdo->prepare("UPDATE users SET first_name = ?, last_name = ?, email = ?, role = ?, active = ? WHERE username = ?");
    $stmt->execute([$_POST['first_name'], $_POST['last_name'], $_POST['email'],$_POST['role'], $_POST['active'], $_POST['username']]);
}

$stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
$stmt->execute([$_REQUEST['username']]);
$user = $stmt->fetch();
$username = $user['username'];

?>
<!DOCTYPE html>
<html>
<head>
    <title>Edit User</title>
    <style>
        body {
            font-family: Georgia, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        h2 {
            margin-top: 20px;
            margin-bottom: 10px;
            text-align: center;
        }

        form {
            max-width: 400px;
            margin: 20px auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        label {
            display: block;
            margin-bottom: 5px;
        }

        input[type="text"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        button[type="submit"],
        button[type="button"] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        button[type="submit"]:hover,
        button[type="button"]:hover {
            background-color: #45a049;
        }

        a {
            text-decoration: none;
        }

        a button[type="button"] {
            background-color: #ccc;
            color: #333;
            margin-right: 10px;
        }

        a button[type="button"]:hover {
            background-color: #bbb;
        }
    </style>
</head>
<body>

<h2>User</h2>

<form action="user.php" method="post">
    <input type='hidden' name=save id=save value=1></input>
    <input type='hidden' name=username id=username value='<?php echo $username ?>'></input>
    <div>
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" id="first_name" required value="<?php echo($user['first_name']) ?>">
    </div>
    <div>
        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" id="last_name" required value="<?php echo($user['last_name']) ?>">
    </div>
    <div>
        <label for="email">Email:</label>
        <input type="text" name="email" id="email" required value="<?php echo($user['email']) ?>">
    </div>
    <div>
        <label for="role">Role:</label>
            <select name="role" id="role">
                <option value="admin" <?php if ($user['role']== 'admin') echo 'selected'; ?>>Admin</option>
                <option value="user" <?php if ($user['role'] == 'user') echo 'selected'; ?>>User</option>
        </select>
    </div>
    <div>
        <label for="active">Active:</label>
            <select name="active" id="active">
                <option value=1 <?php if ($user['active']==1) echo 'selected'; ?>>Active</option>
                <option value=0 <?php if ($user['active'] == 0) echo 'selected'; ?>>Inactive</option>
        </select>
    </div>

    <button type="submit">Save</button>
    <a href='homepage.php'><button type="button">Cancel</button></a>
</form>
<a href='list_tasks.php?username=<?php echo $username?>'><button>View User Tasks</button></a>
</body>
</html>

