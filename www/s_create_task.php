<?php
// Database configuration
require_once('db.php');
require_once('header.php');
require_once('lib.php');
// Assuming you've received the username and password from the login form
$username = $_SESSION['username'];
$title = $_POST['title'];
$description = $_POST['description'];
$status = $_POST['status'];
$priority = $_POST['priority'];
$category = $_POST['category'];
$duedate = $_POST['duedate'];


$stmt = $pdo->prepare("insert into tasks (username,title, description,status,priority,category,duedate) values (?,?,?,?,?,?,?)");
$stmt->execute([$username,$title,$description,$status,$priority,$category,$duedate]);


user_message('Task Created!');


require 'homepage.php';
exit();