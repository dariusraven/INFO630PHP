<?php
require_once('header.php');
require_once('db.php');

$user = '';

if ($_SESSION['role'] == 'admin') {
    $user = $_GET['username'];
} else {
    $user = $_SESSION['username'];
}

$search = '';
if (!empty($_GET['search'])) {
    $search = $_GET['search'];
}

//set default search/pagination variables if not part of get vars
$search = "";
if (!empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
}

$page = 1;
if (!empty($_GET['page'])) {
    $page = $_GET['page'];
}
if ($page < 1){
    $page = 1;
}

$pagesize = 5;
if (!empty($_GET['pagesize'])) {
    $pagesize = $_GET['pagesize'];
}

$completed = 0;
if (!empty($_GET['completed'])) {
    $completed = $_GET['completed'];
}

//fetch tasks from database
$data = get_tasks($user, $search,$completed,$page,$pagesize);
$tasks = $data['tasks'];
$rows = $data['rows'];
$pages = $data['pages'];


?>
<!DOCTYPE html>
<html>
<head>
    <title>Task Table</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<div id='Create_Task_Button'><a href='f_create_task.php'><button>Create New Task</button></a></div>
</br>
<div id='search'>
<form method='post' action='list_tasks.php'>
    <input type=text name='search' id='search' value='<?php echo $search?>'></input>
    <button type='submit'>Search</button>
</form>
</div>

<div class="task-container" id="active_tasks">
    <h2>Active Tasks</h2>
    <table id="activeTaskTable">
        <thead>
            <tr>
                <th onclick="sortTable(0)">Title</th>
                <th onclick="sortTable(1)">Description</th>
                <th onclick="sortTable(2)">Status</th>
                <th onclick="sortTable(3)">Priority</th>
                <th onclick="sortTable(4)">Category</th>
                <th onclick="sortTable(5)"><?php
                    if($completed){echo 'Date Completed';} else{echo 'Due Date';}
                ?></th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php

            foreach ($tasks as $task) {
                $task_id = $task['task_id'];
                echo "<tr>";
                echo "<td>" . htmlspecialchars($task['title']) . "</td>";
                echo "<td>" . htmlspecialchars($task['description']) . "</td>";
                echo "<td>" . htmlspecialchars($task['status']) . "</td>";
                echo "<td>" . htmlspecialchars($task['priority']) . "</td>";
                echo "<td>" . htmlspecialchars($task['category']) . "</td>";
                if(!$completed){
                    echo "<td>" . htmlspecialchars($task['duedate']) . "</td>";
                }
                else{
                    echo "<td>" . htmlspecialchars($task['datecompleted']) . "</td>"; 
                }
                echo "<td>";
                echo '<form action="task.php" method="post">';
                echo "<input type='hidden' name='task_id' id='task_id' value='$task_id'>";
                echo '<button type="submit">Edit</button></form>';
                echo "</td>";
                echo "<td>";
                echo '<form action="s_delete_task.php" method="post">';
                echo "<input type='hidden' name='task_id' value='$task_id'>";
                echo "<button type='submit' onclick=\"return confirm('Are you sure you want to delete this task?');\">Delete</button>";
                echo '</form>';
                echo "</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>



    <div id='pagination'>
        <?php
        echo "Current Page: $page ";
        //only show pagination if more than one page worth of content
        if($pages > 1){
            // show previous page icon if not on first page
            if($page != 1){
                echo "<a href='list_tasks.php?search=".$search."&user=$user&page=".($page-1)."'><< </a>";
            }
            // list links to all pages
            for($x = 1;$x <= $pages; $x++){
                echo "<a href='list_tasks.php?search=".$search."&user=$user&page=$x'> $x</a>";
            }
            // show next page icon if not last page
            if($page != $pages){
                echo "<a href='list_tasks.php?search=".$search."&user=$user&page=".($page+1)."'> >></a>";
            }
        }
        ?>
    </div>
    <?php
    if($completed){
        echo("<div id='toggle'><a href='list_tasks.php?username=$user&completed=0'><button>Show Active Tasks</button></a></div>");
    }
    else{
        echo("<div id='toggle'><a href='list_tasks.php?username=$user&completed=1'><button>Show Completed Tasks</button></a></div>");
    }
    ?>
<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("activeTaskTable");
        switching = true;
        dir = "asc";
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
</script>

</body>
</html>
