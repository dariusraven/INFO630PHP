<?php
require_once('header.php');
?>

<div class="create-task-form-container">
    <h2>Create Task Form</h2>

    <form action="s_create_task.php" method="post">
        <div>
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" required>
        </div>
        <div>
            <label for="description">Description:</label><br>
            <textarea name="description" id="description" maxlength=400 required></textarea>
        </div>
        <div>
            <label for="status">Status:</label>
            <select name="status" id="status">
                <option value="new">New</option>
                <option value="in_progress">In Progress</option>
                <option value="complete">Completed</option>
            </select>
        </div>
        <div>
            <label for="priority">Priority:</label>
            <select name="priority" id="priority">
                <option value="3">Low</option>
                <option value="2">Medium</option>
                <option value="1">High</option>
            </select>
        </div>
        <div>
            <label for="category">Category:</label>
            <select name="category" id="category">
                <option value="personal">Personal</option>
                <option value="urgent">Urgent</option>
                <option value="work">Work</option>
                <option value="school">School</option>
            </select>
            
        </div>
        <div>
        <label for="duedate">Due Date:</label>
        <input type="date" id="duedate" name="duedate" required>
        </div>
        <button type="submit">Create Task</button>
    </form>
</div>