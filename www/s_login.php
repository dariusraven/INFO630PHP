<?php
// Database configuration
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once('db.php');
require_once('lib.php');

// Assuming you've received the username and password from the login form
$username = $_POST['username'];
$password = $_POST['password'];

// Prepared statement to prevent SQL Injection
$stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
$stmt->execute([$username]);
$user = $stmt->fetch();

if ($user) {
    // Check if the user is active
    if ($user['active'] == 1) {
        // Verify the hashed password against the one provided by the user
        if (password_verify($password, $user['passhash'])) {
            user_message("Login successful!");
            $_SESSION['username'] = $username;
            $_SESSION['role'] = $user['role'];

            // Insert session data into the session_table
            $session_id = session_id();
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
            $ip_address = $_SERVER['REMOTE_ADDR'];
            $last_activity = date('Y-m-d H:i:s');
            $stmt = $pdo->prepare("INSERT INTO session_table (session_id, username, user_agent, ip_address, last_activity) VALUES (?, ?, ?, ?, ?)");
            $stmt->execute([$session_id, $username, $user_agent, $ip_address, $last_activity]);

            // Redirect to homepage

            header("Location: homepage.php");
            exit();
        } else {
           user_message("Login failed!");
            include "login.php";
        }
    } else {
        // User is inactive
        user_message("User is inactive.");
        include "inactive.php";
    }
} else {
    user_message("Login failed!");
    include "logout.php";
}
