<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once('db.php');
require_once('lib.php');

$username = $_POST['username'];
$password = $_POST['password'];
$password2 = $_POST['password2'];

if($password != $password2){
    user_message('Passwords do not match');
    require 'f_register.php';
    exit();
}

$stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
$stmt->execute([$username]);
$user = $stmt->fetch();

if ($user) {
    user_message('That username already exists');
    require 'f_register.php';
    exit();
} else {
    $stmt = $pdo->prepare("insert into users (username, passhash,role,first_name,last_name,email) values (?,?,?,?,?,?)");
    $stmt->execute([$username,password_hash($password, PASSWORD_DEFAULT),'user',$_POST['first_name'],$_POST['last_name'],$_POST['email']]);

    send_email('test','test','test');
    user_message('User created, Please login');
    require 'f_login.php';
    exit();
}