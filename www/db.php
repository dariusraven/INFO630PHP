<?php
$host = 'db:3306'; // The service name in the docker-compose file
$db   = 'info630';
$user = 'info630';
$pass = 'info63012345';
//$user = 'root';//'INFO630';
//$pass = '';//'vG4U8uIBQAypPxGK';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo;
try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

function get_pdo(){
    global $pdo;
    return $pdo;
}

?>
