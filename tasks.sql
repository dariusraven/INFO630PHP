-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: db:3306
-- Generation Time: Apr 24, 2024 at 12:27 AM
-- Server version: 5.7.44
-- PHP Version: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `info630`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--
drop table if exists tasks;
CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `category` varchar(20) NOT NULL DEFAULT '',
  `duedate` date DEFAULT NULL,
  `datecompleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `username`, `title`, `description`, `priority`, `status`, `category`, `duedate`, `datecompleted`) VALUES
(1, 'user2', 'test', 'test description', 3, 'new', '', NULL, NULL),
(2, 'user2', 'old task', 'this is an old task', 1, 'in_progress', '', NULL, NULL),
(3, 'user1', 'task one', 'user1 task test', 3, 'new', 'personal', NULL, NULL),
(4, 'user1', 'test', 'user1 task', 3, 'new', '', NULL, NULL),
(5, 'user1', 'test', 'user1 task', 3, 'new', '', NULL, NULL),
(6, 'user1', 'test', 'user1 task', 3, 'new', '', NULL, NULL),
(9, 'AbrinaC', 'Homework', 'Study', 3, 'new', '', NULL, NULL),
(10, 'AbrinaC', 'Cooking', 'Hobby', 3, 'new', '', NULL, NULL),
(11, 'user1', 'Project Plan', 'Project X', 1, 'new', 'work', NULL, NULL),
(12, 'user1', 'work test', 'work test', 3, 'in_progress', 'work', NULL, NULL),
(13, 'user1', 'new', 'new new', 3, 'new', 'personal', NULL, NULL),
(14, 'fullname', 'text area test', 'test test test', 3, 'new', 'personal', NULL, NULL),
(15, 'user1', 'classroom test', 'demo task', 2, 'new', 'school', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;