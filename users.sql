-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: db:3306
-- Generation Time: Apr 03, 2024 at 01:38 AM
-- Server version: 5.7.44
-- PHP Version: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `info630`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
drop table if exists users;
CREATE TABLE `users` (
  `username` varchar(80) NOT NULL,
  `passhash` varchar(256) NOT NULL,
  `role` text NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `email` varchar(120) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `passhash`, `role`, `first_name`, `last_name`, `email`) VALUES
('test', '$2y$10$uQFXcaz38OanVctvUGbgve54b5.E4ATQuRn76heTahP7fk4GA5TPC', 'admin', '', '', ''),
('jburch', '$2y$10$wZ43OGq.BzCvHYbjrUtb7u6/pVCP1Y7D4GnGGfV3PHVS9nbZT4qOu', 'admin', '', '', ''),
('user1', '$2y$10$49Pukw50UNXw0bSr1g8apexEQUALu/Q.eszqUEcpRTDTDQJVi1VEe', 'user', '', '', ''),
('user2', '$2y$10$w50ZTwr5n22ncclwtyIPAuEkY2lihS/V2hibujSejf0SqGM47Pjpm', 'user', '', '', ''),
('testuser1', '$2y$10$TwwRJeuYI48.yZRl6CRAYeRNz.yxN/0NQvTHsGuTJw51yzRpVdtXW', 'user', '', '', ''),
('newuser', '$2y$10$N3uGnZzHB8mYXukb.UoHzOljF8zF1fe/LzTO6N8BKKEy5NIhKKcsq', 'user', '', '', ''),
('fullname', '$2y$10$KyK4FZpgcyKMd9YopUd6pOIZPfLabm5ykEOzA7oy/Plm2LF2bTfAa', 'user', 'Jordan', 'Burch', 'jburch@vcu.edu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);
COMMIT;


alter table users add active INTEGER(1) not null default 1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
