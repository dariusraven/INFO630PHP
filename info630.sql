-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jan 25, 2024 at 05:21 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `info630`
--

-- --------------------------------------------------------

--
-- Table structure for table `session_table`
--

DROP TABLE IF EXISTS `session_table`;
CREATE TABLE IF NOT EXISTS `session_table` (
  `session_id` varchar(120) NOT NULL,
  `username` varchar(80) NOT NULL,
  `user_agent` varchar(160) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `last_activity` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_table`
--

INSERT INTO `session_table` (`session_id`, `username`, `user_agent`, `ip_address`, `last_activity`) VALUES
('5ad5nj2b7n1uduofv1naru9dqa', 'test', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 06:16:44'),
('5ad5nj2b7n1uduofv1naru9dqa', 'test', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 06:18:16'),
('5ad5nj2b7n1uduofv1naru9dqa', 'test', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 06:18:35'),
('5ad5nj2b7n1uduofv1naru9dqa', 'test', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 06:21:55'),
('5ad5nj2b7n1uduofv1naru9dqa', 'test', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 06:25:35'),
('5ad5nj2b7n1uduofv1naru9dqa', 'jburch', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 20:09:23'),
('5ad5nj2b7n1uduofv1naru9dqa', 'jburch', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 20:25:09'),
('5ad5nj2b7n1uduofv1naru9dqa', 'user1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 20:31:43'),
('5ad5nj2b7n1uduofv1naru9dqa', 'jburch', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 20:34:22'),
('5ad5nj2b7n1uduofv1naru9dqa', 'user2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-24 23:15:51'),
('lpso318dkgn8e7ffk3itq176t8', 'user1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-25 04:44:18'),
('lpso318dkgn8e7ffk3itq176t8', 'user1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', '::1', '2024-01-25 05:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `username`, `title`, `description`, `priority`, `status`) VALUES
(1, 'user2', 'test', 'test description', 3, 'new'),
(2, 'user2', 'old task', 'this is an old task', 1, 'in_progress'),
(3, 'user1', 'test', 'user1 task', 3, 'new'),
(4, 'user1', 'test', 'user1 task', 3, 'new'),
(5, 'user1', 'test', 'user1 task', 3, 'new'),
(6, 'user1', 'test', 'user1 task', 3, 'new'),
(7, 'user1', 'test', 'user1 task', 3, 'new'),
(8, 'user1', 'test', 'user1 task', 3, 'new');

-- --------------------------------------------------------

--
-- Table structure for table `tast_category`
--

DROP TABLE IF EXISTS `tast_category`;
CREATE TABLE IF NOT EXISTS `tast_category` (
  `task_id` int(11) NOT NULL,
  `category` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(80) NOT NULL,
  `passhash` varchar(256) NOT NULL,
  `role` text NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `passhash`, `role`) VALUES
('test', '$2y$10$uQFXcaz38OanVctvUGbgve54b5.E4ATQuRn76heTahP7fk4GA5TPC', 'admin'),
('jburch', '$2y$10$wZ43OGq.BzCvHYbjrUtb7u6/pVCP1Y7D4GnGGfV3PHVS9nbZT4qOu', 'admin'),
('user1', '$2y$10$49Pukw50UNXw0bSr1g8apexEQUALu/Q.eszqUEcpRTDTDQJVi1VEe', 'user'),
('user2', '$2y$10$w50ZTwr5n22ncclwtyIPAuEkY2lihS/V2hibujSejf0SqGM47Pjpm', 'user');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
DROP TABLE IF EXISTS `emails`;
create table if not exists `emails`(
`id` INT(11) not null AUTO_INCREMENT,  
`address` VARCHAR(255) not null,
`subject` VARCHAR(300) not null default '',
`body` TEXT not null,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
COMMIT;